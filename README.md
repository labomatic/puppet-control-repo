# puppet-control-repo


## Description

The objective of this repo is to test and define a way to manage nodes using
puppet and bolt tools.

The name is based on puppetlabs
control-repo](https://github.com/puppetlabs/control-repo) template 

## Badges

Todo

## Visuals

Todo

## Installation

You need Puppet [bolt](https://www.puppet.com/docs/bolt/latest/bolt_installing)
installed on your system.

For a container usage, the repository [contains example](containers/README.md)
to add `native-ssh` and `yubikey` support to official images. 

Then, just clone the repository :

      git clone https://gitlab.com/labomatic/puppet-control-repo.git myenv \
      && cd myenv

Test checking the inventory.

    $ bolt inventory show  --detail
    example01
      name: example01                   <----------------(1)
      uri: 127.0.0.1
      alias: []
      config:
        transport: ssh
        ssh:
          batch-mode: true
          cleanup: true
          connect-timeout: 10
          disconnect-timeout: 5
          load-config: true
          login-shell: bash
          tty: false
          host-key-check: false
          native-ssh: true
          tmpdir: "/dev/shm"
          user: root
      vars: {}
      features: []
      facts:
        role: server_sandbox             <-----------------(2) 
      plugin_hooks:
        puppet_library:
          plugin: puppet_agent
          stop_service: true
      groups:
      - server_sandbox
      - all
    
    Inventory source
      /Boltdir/inventory.yaml
    
    Target count
      1 total, 1 from inventory, 0 adhoc
    
    Additional information
      Use the '--targets', '--query', or '--rerun' option to view specific targets

- (1)(2): host declared in [inventory/server_sandbox.yaml](inventory/server_sandbox.yaml)
- (2): data collected with [_plugin: role_plugin](modules/role_plugin/README.md)
       in [inventory.yaml](inventory.yaml)

## Usage

Create your [roles and profiles](https://www.puppet.com/docs/puppet/8/designing_system_configs_roles_and_profiles)
modules using puppet code.

Declare your hosts in `inventory/<rolename>.yaml` 

Test the code 

     bolt apply  manifests/site.pp -t <yourhost|hostgroup> --noop

Apply the code 

     bolt apply  manifests/site.pp -t <yourhost|hostgroup>


@see `bolt apply --help`


## Support

Todo

## Roadmap

Todo

## Contributing

Common pull request workflow.

## Authors and acknowledgment

Todo

## License

GNU GENERAL PUBLIC LICENSE Version 3, 29 June 2007

## Project status

Developpement
