# Presentation

This document shows how to use puppetlabs bolt with rootless podman with
native-ssh and yubikey support.

# Allow native-ssh

Because the official puppetlabs image doesn't contain ssh command needed for
native-ssh support, a way is to create own images from puppetlabs one.

Using the [Dockerfile](bolt/Dockerfile) present in this directory, just run

    podman build --tag localhost/puppet-bolt:openssh bolt

or

    podman-compose build bolt

And try it


    $ podman run -ti --rm localhost/puppet-bolt:openssh 
                 `.::-`
            `.-:///////-.`
         `-:////:.  `-:///:-  /ooo.                        .ooo/
     `.-:///::///:-`   `-//:  ymmm-                        :mmmy  .---.
    :///:-.   `.:////.  -//:  ymmm-                        :mmmy  +mmm+
    ://.          ///.  -//:  ymmm--/++/-       `-/++/:`   :mmmy-:smmms::-
    ://.          ://. .://:  ymmmdmmmmmmdo`  .smmmmmmmmh: :mmmysmmmmmmmms
    ://.          ://:///:-.  ymmmh/--/hmmmy -mmmd/-.:hmmm+:mmmy.-smmms--.
    ://:.`      .-////:-`     ymmm-     ymmm:hmmm-    `dmmm/mmmy  +mmm+
    `-:///:-..:///:-.`        ymmm-     ommm/dmmm`     hmmm+mmmy  +mmm+
       `.-:////:-`            ymmm+    /mmmm.ommms`   /mmmh:mmmy  +mmmo
           `-.`               ymmmmmhhmmmmd:  ommmmhydmmmy`:mmmy  -mmmmdhd
                              oyyy+shddhs/`    .+shddhy+-  -yyyo   .ohddhs
    
    
    🎉 Welcome to Bolt 3.27.4
    😌 We're here to help bring order to the chaos
    📖 Find our documentation at https://bolt.guide
    🙋 Ask a question in #bolt on https://slack.puppet.com/
    🔩 Contribute at https://github.com/puppetlabs/bolt/
    💡 Not sure where to start? Try "bolt command run 'hostname' --target localhost"
    
    We only print this message once. Run "bolt help" again for help text.

# Create a shell function to use it.

To use bolt as a commandline, it's necessary to mount local path to access
environment data such ad configuration, inventory, puppet code, plans, tasks ...

- **~/.ssh**: for ssh keys
- **~/.ssh/sockets**: for ssh ControleMaster sockets
- **~/.puppetlabs**: for puppet common confgurations.
- **~/.gnupg**: for agent for ubikey
- **$SSH_AUTH_SOCK**: for agent for yubikey
- **bolt dir**: for data.

A way to simplify the commandline use is to create a function and a bolt alias.

Put the following code in `"$HOME"/.functions` _(for example)_ and source the
file in your `~/.bashrc` _(are whateverrc file)_.

```bash
PODMAN_REPO_PREFIX=docker.io

docker_bolt(){

	local boltdir
	local networkopts
	local entrypointopts

	boltdir="$(realpath .)"
	networkopts=
	entrypointopts=

	while [ "${#}" -gt "0" ]
	do
		case "$1" in
			"--shell"|"--interactive")
				entrypointopts="--entrypoint bash"
				;;
			"--boltdir")
				boltdir="$(realpath "$2" 2>/dev/null)"
				shift
				shift
				[ -z "${boltdir}" ] && {
					printf -- "[-] param --boltdir needs a parameter\n"
					return 1
				}
				[ ! -d "${boltdir}" ] && {
					printf -- "[-] $boltdir does not exist\n"
					return 1
				}
				;;
			"--network")
				[ -z "${2}" ] && {
					printf -- "[-] param --network needs a parameter\n"
					return 1
				}
				networkopts="--network $2"
				shift
				shift
				;;
			*)
				break
				;;
		esac
	done

	# Create tmpdir for master socket.
	bolt_tmp_dir=/dev/shm/bolt-"$USER"
	[ -d  "${bolt_tmp_dir}" ] || /usr/bin/mkdir "${bolt_tmp_dir}"
	chmod 700 "${bolt_tmp_dir}"

	SSH_SOCKET_PATH="$(/usr/bin/mktemp --directory --tmpdir=/dev/shm/bolt-"$USER")"
	[ -d "${SSH_SOCKET_PATH}" ] || {
		printf -- "[-] error, unable to create socket path %s\n" "${SSH_SOCKET_PATH}"
		return 1
	}
	chmod 700 "${SSH_SOCKET_PATH}"

	HOMEDIR=/root
	podman run --rm -it \
		--name  bolt\
		--workdir /Boltdir \
		$networkopts \
		$entrypointopts \
		--mount type=bind,source="$HOME"/.puppetlabs,destination="$HOMEDIR"/.puppetlabs \
		--mount type=bind,source="$HOME"/.ssh,destination="$HOMEDIR"/.ssh,ro \
		--mount type=bind,source="${SSH_SOCKET_PATH}",destination="$HOMEDIR"/.ssh/sockets \
		--mount type=bind,source="$HOME"/.gnupg,destination="$HOMEDIR"/.gnupg,ro \
		--mount type=bind,source="${boltdir}",destination=/Boltdir \
		--mount type=bind,source="$(readlink -f $SSH_AUTH_SOCK)",destination=/ssh-agent,ro \
		-e SSH_AUTH_SOCK=/ssh-agent \
		localhost/puppet-bolt:openssh "$@"

	/usr/bin/rm -rf "${SSH_SOCKET_PATH}"
}

alias bolt=docker_bolt
```

Source your file or run new terminal.

# Usage examples

## Default, get help
Use it simply

    $ bolt 
    Name
        bolt
    
    Usage
        bolt <subcommand> [action] [options]
    
    Description
        Bolt is an orchestration tool that automates the manual work it takes to
        maintain your infrastructure.
    
    Subcommands
        apply             Apply Puppet manifest code
        command           Run a command remotely
        file              Copy files between the controller and targets
        group             Show the list of groups in the inventory
        guide             View guides for Bolt concepts and features
        inventory         Show the list of targets an action would run on
        module            Manage Bolt project modules
        lookup            Look up a value with Hiera
        plan              Convert, create, show, and run Bolt plans
        plugin            Show available plugins
        policy            Apply, create, and show policies
        project           Create and migrate Bolt projects
        script            Upload a local script and run it remotely
        secret            Create encryption keys and encrypt and decrypt values
        task              Show and run Bolt tasks
    
    Guides
        For a list of guides on Bolt's concepts and features, run 'bolt guide'.
        Find Bolt's documentation at https://bolt.guide.
    
    Global options
        -h, --help                       Display help.
            --version                    Display the version.
            --log-level LEVEL            Set the log level for the console. Available options are
                                         trace, debug, info, warn, error, fatal.
            --clear-cache                Clear plugin, plan, and task caches before executing.


## Enter container

To use run a bolt container and get a prompt for testing

    bolt --shell


## Access container network

to access running container network, list networks then use `--network`
parameter from `bolt` alias


## All in one lab


The [compose](container-compose.yaml) file runs a complete test:

- run 2 sshd container pointing on local authorized_keys file for automatic ssh
- run bolt container executing a command on the 2 clients


Bolt container uses ssh connection to access clients containers. To allow
automatic access using ssh key,

- create a testing key with `ssh-keygen`
- create `tmp/ssh-config` dir _(cf volumes in [container-compose.yaml](container-compose.yaml))_
- copy public key in `tmp/ssh-config/authotized_keys` file.

_client1 and clie$nt2 are declared in inventory/server_sandbox.yaml_

For the impatients, just run `podman-compose up` in current dir.

```console
podman-compose version: 1.0.6
['podman', '--version', '']
using podman version: 4.8.1
** excluding:  set()
['podman', 'inspect', '-t', 'image', '-f', '{{.Id}}', 'localhost/almalinux9:sshd']
['podman', 'inspect', '-t', 'image', '-f', '{{.Id}}', 'localhost/almalinux9:sshd']
['podman', 'inspect', '-t', 'image', '-f', '{{.Id}}', 'localhost/puppet-bolt:openssh']
['podman', 'ps', '--filter', 'label=io.podman.compose.project=boltlab', '-a', '--format', '{{ index .Labels "io.podman.compose.config-hash"}}']
['podman', 'network', 'exists', 'boltlab_default']
podman create --name=boltlab_client1_1 --label io.podman.compose.config-hash=f43cfa8c32838e1c2063cf8701316487299c1788868cfe184cf8834384ad3359 --label io.podman.compose.project=boltlab --label io.podman.compose.version=1.0.6 --label PODMAN_SYSTEMD_UNIT=podman-compose@boltlab.service --label com.docker.compose.project=boltlab --label com.docker.compose.project.working_dir=/home/fccagou/code/bolt/lab/containers --label com.docker.compose.project.config_files=container-compose.yaml --label com.docker.compose.container-number=1 --label com.docker.compose.service=client1 -v ./tmp/ssh-config:/root/.ssh --net boltlab_default --network-alias client1 localhost/almalinux9:sshd
410c15d28e704d398f5c7d2b9290302bf1c56c92451e3f9aa3b7c93ed22c7b0a
exit code: 0
['podman', 'network', 'exists', 'boltlab_default']
podman create --name=boltlab_client2_1 --label io.podman.compose.config-hash=f43cfa8c32838e1c2063cf8701316487299c1788868cfe184cf8834384ad3359 --label io.podman.compose.project=boltlab --label io.podman.compose.version=1.0.6 --label PODMAN_SYSTEMD_UNIT=podman-compose@boltlab.service --label com.docker.compose.project=boltlab --label com.docker.compose.project.working_dir=/home/fccagou/code/bolt/lab/containers --label com.docker.compose.project.config_files=container-compose.yaml --label com.docker.compose.container-number=1 --label com.docker.compose.service=client2 -v ./tmp/ssh-config:/root/.ssh --net boltlab_default --network-alias client2 localhost/almalinux9:sshd
c949371d984a40ceb3a23de1a28fa60ebd346ac285484198728562e49c1a4e15
exit code: 0
['podman', 'network', 'exists', 'boltlab_default']
podman create --name=boltlab_bolt_1 --requires=boltlab_client2_1,boltlab_client1_1 --label io.podman.compose.config-hash=f43cfa8c32838e1c2063cf8701316487299c1788868cfe184cf8834384ad3359 --label io.podman.compose.project=boltlab --label io.podman.compose.version=1.0.6 --label PODMAN_SYSTEMD_UNIT=podman-compose@boltlab.service --label com.docker.compose.project=boltlab --label com.docker.compose.project.working_dir=/home/fccagou/code/bolt/lab/containers --label com.docker.compose.project.config_files=container-compose.yaml --label com.docker.compose.container-number=1 --label com.docker.compose.service=bolt -e SSH_AUTH_SOCK=/ssh-agent -v /home/fccagou/.puppetlabs:/root/.puppetlabs -v /home/fccagou/.ssh:/root/.ssh -v /dev/shm:/root/.ssh/sockets -v /home/fccagou/.gnupg:/root/.gnupg -v ../.:/Boltdir -v /run/user/1000/gnupg/S.gpg-agent.ssh:/ssh-agent --net boltlab_default --network-alias bolt -w /Boltdir localhost/puppet-bolt:openssh command run -t client* hostname -f
b0b8bff11580f81bc6d985b070e36a299012434662dd693f42a422bf83544e16
exit code: 0
podman start -a boltlab_client1_1
podman start -a boltlab_client2_1
podman start -a boltlab_bolt_1
[bolt]    | Started on client1...
[bolt]    | Started on client2...
[bolt]    | Finished on client2:
[bolt]    |   Warning: Permanently added 'client2' (ED25519) to the list of known hosts.
[bolt]    |   c949371d984a
[bolt]    | Finished on client1:
[bolt]    |   Warning: Permanently added 'client1' (ED25519) to the list of known hosts.
[bolt]    |   410c15d28e70
[bolt]    | Successful on 2 targets: client1,client2
[bolt]    | Ran on 2 targets in 0.74 sec
exit code: 0
```


For unit testing, run testing containers using compose:

```console
$ podman-compose up client1 client2
....
podman start -a boltlab_client1_1
podman start -a boltlab_client2_1
```

List existing networks:

```console
$ podman network ls
NETWORK ID    NAME         DRIVER
c794b450fec8  boltlab_default  bridge
2f259bab93aa  podman       bridge
3294411061f0  puppet       bridge
```


```console
$ bolt --network boltlab_default command run -t 'client*' 'hostname -f'
Started on client1...
Started on client2...
Finished on client1:
  Warning: Permanently added 'client1' (ED25519) to the list of known hosts.
  ffc8fea92f57
Finished on client2:
  Warning: Permanently added 'client2' (ED25519) to the list of known hosts.
  a597bce2a4eb
Successful on 2 targets: client1,client2
Ran on 2 targets in 0.66 sec
```

