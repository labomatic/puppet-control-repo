#!/usr/bin/bash
#

echo 'root:toortoor' | chpasswd

[ -f /etc/sysconfig/sshd ] && source /etc/sysconfig/sshd

for ktype in rsa ecdsa ed25519
do
	[ -f /etc/ssh/ssh_host_"$ktype"_key ] ||
		/usr/libexec/openssh/sshd-keygen "$ktype"
done

/usr/sbin/sshd -D $OPTIONS

