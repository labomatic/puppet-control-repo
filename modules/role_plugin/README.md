# Description

This module contains a [bolt inventory
plugin](https://www.puppet.com/docs/bolt/latest/writing_plugins) used to
generate groups based on existing roles.

For each role in `site/role/manifest`, the task create an inventory group entry
in the form:

    - name: rolexxxx
      targets:
        _plugin: yaml
        filepath: inventory/rolexxx.yaml
      facts:
        role: rolexxx

The inventory/*.yaml files contain array of
[targets](https://www.puppet.com/docs/bolt/latest/bolt_inventory_reference#targets-1)

The objectives are:

- New role is automatically declared
- The `inventory/*.yaml` stay simple to add/modifiy/del nodes.
- Using the `role` fact in `hiera.yaml`

# How to use it

This module must be installed in bolt `modulepath`.

The puppet environment must contain
- `site/role` module with all roles
- `ìnventory`directory for nodes declarations.

Add the next in inventory file groups declaration 

    groups:
    ....
    - _plugin: role_plugin
    ....

It can be useful to use put `inventory/*.yaml` files in other place because of
rights access. The plugin parameter `inventorydir` can be set in fullpath
format.

    groups:
    ....
    - _plugin: role_plugin
      inventorydir: /srv/inventory
    ....

# Testing

For unit test, use the command 


    $ PT__format=yaml sh modules/role_plugin/tasks/resolve_reference.sh 
    - name: server_sandbox
      targets:
        _plugin: yaml
        filepath: inventory/server_sandbox.yaml
      facts:
        role: server_sandbox
    
    - name: server_system
      targets:
        _plugin: yaml
        filepath: inventory/server_system.yaml
      facts:
        role: server_system


Bolt inventory test

    $ bolt inventory show -t dobby --detail 
    dobby
      name: dobby
      uri: 1.2.3.4
      alias: []
      config:
        transport: ssh
        ssh:
          batch-mode: true
          cleanup: true
          connect-timeout: 10
          disconnect-timeout: 5
          load-config: true
          login-shell: bash
          tty: false
          host-key-check: false
          native-ssh: true
          tmpdir: "/dev/shm"
          user: root
      vars: {}
      features: []
      facts:
        role: server_sandbox
      plugin_hooks:
        puppet_library:
          plugin: puppet_agent
          stop_service: true
      groups:
      - server_sandbox
      - all
    
    Inventory source
      /Boltdir/inventory.yaml
    
    Target count
      1 total, 1 from inventory, 0 adhoc


