#!/usr/bin/bash
#
#
# Security tags
#
set -o pipefail
set -o errexit
set -o nounset

#
# Common finctions
error() {
	printf -- "[-] %s\n" "$@"
}

#
# Function used by the plugin's task.
#
# The task must display a json containing a value key.
#
# {
#    "value": xxxxxxx
# }
#
task_result() {
	local inventorydir
	inventorydir="${PT__inventorydir:-$boltdir/inventory}"

	cat <<EOF_HEADER
{
  "_infos": {
    "date": "$(date)",
	"id": "$(id -un)",
    "hostname": "$(hostname)"
  },
  "value": [

EOF_HEADER

	for role in *.pp
	do
		role="${role//.pp}"
		filepath="$inventorydir/$role.yaml"
		[ -f "$filepath" ] || filepath=/dev/null
		# touch "$boltdir/$filepath"

		cat <<EOF_ROLE
{
  "name": "$role",
  "targets": {
    "_plugin": "yaml",
    "filepath": "$filepath"
  },
  "facts": {
    "role": "$role"
  }
},
EOF_ROLE
	done

cat <<EOF_FOOTER
{ "name": "_dummy" }
]
}
EOF_FOOTER

}

#
# Function for debugging.
#
yaml_result () {
	# Display yaml version.

	for role in *.pp
	do
		role="${role//.pp}"
		filepath="inventory/$role.yaml"
		[ -f "$boltdir/$filepath" ] || filepath=/dev/null
		# touch "$boltdir/$filepath"

		cat <<EOF_ROLE
- name: $role
  targets:
    _plugin: yaml
    filepath: $filepath
  facts:
    role: $role

EOF_ROLE
	done

}


# ===========================================================================
#  Main content
# ===========================================================================
#
# Describes in the json task, the input_method is set to environment so that the
# parameter are all prefixed by "PT__".
#
# The control repo must have his roles in site/role module.
#
boltdir="${PT__boltdir:-$(pwd)}"
pushd site/role/manifests/ > /dev/null || {
	error "getting roles in site/role/manifests"
    exit 1
}

# Get the format parameter.
PT__format="${PT__format:-json}"
case "${PT__format}" in
	yaml) yaml_result ;;
	*) task_result ;;
esac

exit 0



