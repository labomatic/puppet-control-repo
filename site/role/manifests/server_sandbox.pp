# @summary A short summary of the purpose of this class
#
# A description of what this class does
#
# @example
#   include role::server_sandbox
class role::server_sandbox {
  notify{"I'm a ${role}": }
  include profile::system
}
