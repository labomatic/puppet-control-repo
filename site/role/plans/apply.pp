#
#
plan role::apply (
  TargetSpec $targets,
)
{
  get_targets($targets).each |$target| {
    $_rolename=$target.facts['role']
    notice("Applying role::${_rolename}")
    apply($target) { include "role::$_rolename"}
  }
}
